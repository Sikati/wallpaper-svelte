package main

import (
	// "fmt"
	"log"
	"os"
	"os/exec"
)

func updateWallpaper(input string) {
	temp := "../svelte/public/assets/" + input + ".jpg"
	command := exec.Command("output * bg " + temp + " fill")

	command.Stdout = os.Stdout

	err := command.Run()
	if err != nil {
		log.Fatal(err)
	}
}
