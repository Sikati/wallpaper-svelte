package main

import (
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

func main() {
	Router := gin.Default()

	Router.POST("/postwall", GetPost)

	Router.Run(":8080")
}

type Input struct {
	Path string `json:"path"`
}


func GetPost(context *gin.Context) {
	var input Input
	err := context.BindJSON(&input)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(input)
	context.IndentedJSON(http.StatusCreated, input)

	updateWallpaper(input.Path)
}
